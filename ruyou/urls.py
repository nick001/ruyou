from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from hotel import views_api
from hotel.views import Hotels, HotelByName, CheckIn, Sales


router_hotels = routers.SimpleRouter(trailing_slash=False)
router_hotels.register(r'hotels', views_api.HotelsViewSet, basename='hotel')
router_rooms = routers.SimpleRouter(trailing_slash=False)
router_rooms.register(r'rooms', views_api.RoomsViewSet, basename='room')
router_grades = routers.SimpleRouter(trailing_slash=False)
router_grades.register(r'grades', views_api.GradesViewSet, basename='grade')
api_urls = include(router_hotels.urls + router_rooms.urls + router_grades.urls)


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api.', api_urls),
    url(r'^hotels', Hotels.as_view(), name='hotels'),
    url(r'^hotel', HotelByName.as_view(), name='hotel'),
    url(r'^check', CheckIn.as_view(), name='check'),
    url(r'^sales', Sales.as_view(), name='sales'),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
]
