from django.core.management import call_command
from django.db import migrations


def load_fixture(apps, schema_editor):
    fixtures = ['initial_data.json']
    for fixture in fixtures:
        call_command('loaddata', fixture, app_label='hotel')


class Migration(migrations.Migration):
    dependencies = [
        ('hotel', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_fixture),
    ]
