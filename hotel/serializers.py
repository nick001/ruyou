from rest_framework import serializers

from hotel.models import Hotel, Room, Grade



class AllRoomsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Room
        fields = ['url', 'number', 'hotel']


class DetailedGradeSerializer(serializers.HyperlinkedModelSerializer):
    rooms = AllRoomsSerializer(many=True)

    class Meta:
        model = Grade
        fields = ['url', 'name', 'description', 'rooms']


class DetailedRoomSerializer(serializers.HyperlinkedModelSerializer):
    grade = DetailedGradeSerializer()

    class Meta:
        model = Room
        fields = ['url', 'number', 'grade']


class AllGradesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Grade
        fields = ['url', 'name', 'description', 'price', 'hotel']


class DetailedHotelSerializer(serializers.HyperlinkedModelSerializer):
    rooms = AllRoomsSerializer(many=True)
    grades = AllGradesSerializer(many=True)

    class Meta:
        model = Hotel
        fields = ['url', 'name', 'description', 'grades','rooms']


class AllHotelsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hotel
        fields = ['url', 'name']
