from django.contrib import admin

from hotel.models import Hotel, Room, AdditionalService, Grade, Check


@admin.register(Hotel, Room, AdditionalService, Grade, Check)
class AdminNameSearch(admin.ModelAdmin):
    search_fields = ['name']
