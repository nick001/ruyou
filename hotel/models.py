from datetime import datetime

from django.db import models


class NameDescriptionMixin(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()

    class Meta:
        abstract = True


class Hotel(NameDescriptionMixin, models.Model):
    @property
    def rooms(self):
        return Room.objects.filter(hotel=self)

    @property
    def grades(self):
        return Grade.objects.filter(hotel=self)


class Grade(NameDescriptionMixin, models.Model):
    price = models.FloatField()
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)

    @property
    def rooms(self):
        return Room.objects.filter(hotel=self.hotel, grade=self)


class AdditionalService(NameDescriptionMixin, models.Model):
    price = models.FloatField()
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)


class Room(models.Model):
    number = models.IntegerField()
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    grade = models.ForeignKey(
        Grade, on_delete=models.SET_NULL, null=True, blank=True)
    additional_service = models.ForeignKey(
        AdditionalService, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        unique_together = ('number', 'hotel')


class Check(models.Model):
    check_in = models.DateTimeField()
    check_out = models.DateTimeField()
    full_days = models.PositiveIntegerField(default=0)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    @classmethod
    def try_book(cls, hotel_pk, grade_pk, arrive, leave):
        reserved_rooms = cls.objects.exclude(
            check_in__lte=arrive, check_out__lte=arrive
        ).exclude(
            check_in__gte=leave, check_out__gte=leave
        )

        free_rooms = Room.objects.filter(
            hotel=Hotel.objects.get(pk=hotel_pk),
            grade=Grade.objects.get(pk=grade_pk)
        ).exclude(
            id__in=reserved_rooms.values('room')
        )

        status = 0
        if free_rooms:
            new_check = cls(
                check_in=arrive, check_out=leave, room=free_rooms[0])
            new_check.save()
            status = 1

        return status

    @classmethod
    def get_sales(cls, hotel, start, end):
        return cls.objects.filter(
            check_in__gte=start, check_out__lte=end, room__hotel=hotel)

    def save(self, *args, **kwargs):
        if isinstance(self.check_in, str) or isinstance(self.check_out, str):
            datetime_format = '%Y-%m-%d'
            self.check_in = datetime.strptime(
                '{}'.format(self.check_in), datetime_format)
            self.check_out = datetime.strptime(
                '{}'.format(self.check_out), datetime_format)

        self.full_days = (self.check_out - self.check_in).days
        super().save(*args, **kwargs)
