from django.db.models import F, FloatField, Sum
from django.shortcuts import redirect
from django.views.generic import TemplateView

from hotel.models import Hotel, Check


class Hotels(TemplateView):
    template_name = 'hotels.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hotels'] = Hotel.objects.all()

        return context


class HotelByName(TemplateView):
    template_name = 'hotel.html'

    def get(self, request, *args, **kwargs):
        kwargs['pk'] = request.GET.get('pk')
        kwargs['check_status'] = int(request.GET.get('check_status', 0))

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hotel'] = Hotel.objects.get(pk=kwargs['pk'])
        context['check_status'] = kwargs['check_status']

        return context


class CheckIn(TemplateView):
    def get(self, request, *args, **kwargs):
        kwargs['hotel_pk'] = request.GET.get('hotel_pk')

        check_status = Check.try_book(
            kwargs['hotel_pk'], request.GET.get('grade_pk'),
            request.GET.get('arrive'), request.GET.get('leave')
        )

        return redirect(
            '/hotel?pk={}&check_status={}'.format(
                kwargs['hotel_pk'], check_status
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hotel'] = Hotel.objects.get(pk=kwargs['pk'])

        return context


class Sales(TemplateView):
    template_name = 'sales.html'

    def get(self, request, *args, **kwargs):
        kwargs['hotel_pk'] = request.GET.get('hotel_pk')
        start_date = request.GET.get('start', None)
        end_date = request.GET.get('end', None)
        if start_date and end_date:
            kwargs['start'] = start_date
            kwargs['end'] = end_date

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['hotel'] = Hotel.objects.get(pk=kwargs['hotel_pk'])

        if 'start' in kwargs.keys() and 'end' in kwargs.keys():
            sales = Check.get_sales(
                context['hotel'], kwargs['start'], kwargs['end'])

            context['total_price'] = sales.aggregate(
                total=Sum(
                    F('full_days') * F('room__grade__price'),
                    output_field=FloatField()
                )
            )['total']

            all_grades = set(sales.values_list('room__grade__name'))
            context['total_grades'] = dict()
            for grade in all_grades:
                grade = grade[0]
                context['total_grades'][grade] = sales.filter(
                    room__grade__name=grade
                ).aggregate(
                    total=Sum('full_days')
                )['total']

        return context
