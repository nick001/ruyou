from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.settings import api_settings

from hotel.models import Hotel, Room, Grade
from hotel.serializers import (
    DetailedHotelSerializer, AllHotelsSerializer, AllRoomsSerializer,
    DetailedRoomSerializer, DetailedGradeSerializer, AllGradesSerializer
)


class BaseViewSet(viewsets.ViewSet):
    model_class = None
    all_objects_serializer = None
    detailed_serializer = None

    def list(self, request):
        paginator = api_settings.DEFAULT_PAGINATION_CLASS()
        queryset = self.model_class.objects.all()
        tmp_object = paginator.paginate_queryset(queryset, request)
        serializer_class = self.all_objects_serializer(
            tmp_object, many=True, context={'request': request})

        return paginator.get_paginated_response(serializer_class.data)

    def retrieve(self, request, pk=None):
        queryset = self.model_class.objects.all()
        tmp_object = get_object_or_404(queryset, pk=pk)
        serializer = self.detailed_serializer(
            tmp_object, context={'request': request})

        return Response(serializer.data)


class HotelsViewSet(BaseViewSet):
    model_class = Hotel
    all_objects_serializer = AllHotelsSerializer
    detailed_serializer = DetailedHotelSerializer


class RoomsViewSet(BaseViewSet):
    model_class = Room
    all_objects_serializer = AllRoomsSerializer
    detailed_serializer = DetailedRoomSerializer


class GradesViewSet(BaseViewSet):
    model_class = Grade
    all_objects_serializer = AllGradesSerializer
    detailed_serializer = DetailedGradeSerializer
